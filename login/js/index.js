
// firebase auth change state
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        console.log('User is signed in');
        // user is signed in
        location.assign('dashboard.html')
    } else {
        console.log('User is signed out');
        // user is signed out

    }
});

// function for showing the signup div
function showsignup() {
    var $ = jQuery;
    $('#sign-up-div').show();
    $('#login-div').hide();
}

// function for showing the login div
function showsignin() {
    var $ = jQuery;
    $('#sign-up-div').hide();
    $('#login-div').show();
}

//firebase sign in function
function signin() {
    var email = $('#email-signin').val();
    var pass = $('#password-signin').val();
    // console.log(email);
    // console.log(pass);
    firebase.auth().signInWithEmailAndPassword(email, pass)
    .then((userCredential) => {
        // Signed in
        var user = userCredential.user;

        // ...
    }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email the user entered is incorrect.

        if (errorCode == "auth/invalid-email") {
            console.log("The email address is incorrect.");
            Swal.fire({
                title: 'Error!',
                text: errorMessage,
                icon: 'error',
                confirmButtonText: 'ok'
            })
        } else if (errorCode == "auth/wrong-password") {
            console.log("The password is incorrect.");
            Swal.fire({
                title: 'Error!',
                text: "Invalid password or password should be at least 6 letters",
                icon: 'error',
                confirmButtonText: 'ok'
            })
        } else {
            console.log("Error: " + errorCode + " " + errorMessage);
            Swal.fire({
                title: 'Error!',
                text: errorMessage,
                icon: 'error',
                confirmButtonText: 'ok'
            })
        }
    });
}

// firebase signup function
function signup() {
    var email = $('#email-signup').val();
    var pass = $('#password-signup').val();
    var name = $('#name-signup').val();

    //setting name value in localstorage
    localStorage.setItem('user_name',name);
    
    firebase.auth().createUserWithEmailAndPassword(email, pass)
    .then((userCredential) => {
        // Signed in

    }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email the user entered is incorrect.

        if (errorCode == "auth/email-already-in-use") {
            console.log("The email address is already in use.");
            Swal.fire({
                title: 'Error!',
                text: errorMessage,
                icon: 'error',
                confirmButtonText: 'ok'
            })
        } else {
            console.log("Error: " + errorCode + " " + errorMessage);
            Swal.fire({
                title: 'Error!',
                text: errorMessage,
                icon: 'error',
                confirmButtonText: 'ok'
            })
        }
    });
}