var $ = jQuery;
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // user is signed in
    //firebase auth current user
    var user = firebase.auth().currentUser
    //getting the user's email
    var email = user.email
    //getting the user's name
    var name = user.displayName
    $("#avatar-name-user").text(name);
    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();
    
    var output = d.getFullYear() + '/' +
        ((''+month).length<2 ? '0' : '') + month + '/' +
        ((''+day).length<2 ? '0' : '') + day;
    
    $("#avatar-date").text(output);
    console.log(name.split(" ")[0]);
    console.log(name.split(" ")[1]);
    $("#ava-nm-fir").text(name.split(" ")[0]);
    $("#ava-nm-last").text(name.split(" ")[1]);
    $("#ava-email").text(email);
    $("#ava-email").attr("href", "mailto:" + email);
    
    if (name === "null") {
      updateName()
    }

    if (name != "null"){
      //inner html name
      $('#user-name-dropdown').html(name);
    }
    else {
      //inner html name
      $('#user-name-dropdown').html("user");
    }

    if(email != "null"){
      $('#user-email-dropdown').html(email);
    }
    else {
      $('#user-email-dropdown').html(name+"@indiakart.bitbucket.io");
    }

  } else {
    // user is signed out
    location.assign("index.html");
  }
});

//signout firebase function
function signout() {
  firebase.auth().signOut().then(function () {
    location.assign("index.html");
  });
}

//telling a fake price of shipping with the use of Sweet alert popups
function showPrice() {
  Swal.fire({
    title: "Price of shipping",
    text: "The price of shipping according to the location given is is ₹5,000",
    icon: "success",
    confirmButtonText: "Ok"
  });
}

function updateName(){
  name_user_val = localStorage.getItem('user_name')
  if (name_user_val === "null") {
    var user = firebase.auth().currentUser;
    //setting the name of the user firebase
    user.updateProfile({
        displayName: "user",
    })
  }
  else {
    var user = firebase.auth().currentUser;
    //setting the name of the user firebase
    user.updateProfile({
        displayName: name_user_val,
    })
  }
}